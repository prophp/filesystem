<?php

use ProPhp\Mirror\TestsLite\Helper;
use ProPhp\Filesystem\Files;

$location = Helper::getLocation();

$actualOutput = json_encode(Files::list(Helper::getDataDirPath($location) . "/input"), JSON_PRETTY_PRINT);

$expectedOutput = Helper::getOutputData($location);

Helper::assertEquals($actualOutput, $expectedOutput);