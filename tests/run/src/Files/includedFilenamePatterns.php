<?php

use ProPhp\Mirror\TestsLite\Helper;
use ProPhp\Filesystem\Files;
use ProPhp\Filesystem\FilesParams;

$location = Helper::getLocation();

$actualOutput = json_encode(
    Files::list(
        dirname(Helper::getDataDirPath($location)) . "/index/input",
        (new FilesParams())
            ->includedFilenamePatterns([
                "*/dir/*"
            ])
    )
    , JSON_PRETTY_PRINT
);

$expectedOutput = Helper::getOutputData($location);

Helper::assertEquals($actualOutput, $expectedOutput);